<?php

function uc_ewrf_views_data() {
  $data['uc_ewrf']['table']['group'] = t('Recycling Fee');

  $data['uc_ewrf']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  // Přidám do Views recyklační poplatek, je to prostě hustý!
  $data['uc_ewrf']['fee'] = array(
    'title' => t('Recycling Fee'),
    'help' => t('Electronic Waste Recycling Fee'),
    'field' => array(
      'handler' => 'uc_ewrf_handler_field_ewrf',
      'click sortable' => TRUE,
      'float' => TRUE,
      'additional fields' => array(
        'nid' => array(
          'field' => 'nid',
        ),
      ),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_float',
    ),
  );  
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function uc_ewrf_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_ewrf') .'/views',
    ),
    'handlers' => array(
      'uc_ewrf_handler_field_price' => array(
        'parent' => 'uc_product_handler_field_price',
      ),
      'uc_ewrf_handler_field_ewrf' => array(
        'parent' => 'uc_product_handler_field_price',
      ),
    ),
  );
}

function uc_ewrf_views_data_alter(&$data) {
  $data['uc_products']['sell_price']['field']['handler'] = 'uc_ewrf_handler_field_price';
}