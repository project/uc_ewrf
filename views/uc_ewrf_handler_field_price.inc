<?php

/**
 * Return a formatted price value to display in the View.
 */
class uc_ewrf_handler_field_price extends uc_product_handler_field_price {
  function render($values) {
    if ($this->options['format'] == 'numeric') {
      return parent::render($values);
    }

    if ($this->options['format'] == 'uc_price') {
      if (uc_ewrf_include_to_price() && uc_ewrf_get($values->{$this->aliases['nid']})) {
        $values->{$this->field_alias} = uc_ewrf_calculate($values->{$this->aliases['nid']}, $values->{$this->field_alias});
      }
      if (module_exists('uc_dph')) {
        return uc_dph_prices_for_views($values->{$this->field_alias});
      }
      return parent::render($values);
    }
  }
}