<?php

class uc_ewrf_handler_field_ewrf extends uc_product_handler_field_price {
  function render($values) {    
    if ($values->{$this->field_alias} != 0) {
      if (module_exists('uc_dph')) {
        return uc_dph_prices_for_views($values->{$this->field_alias});
      }
      return uc_price($values->{$this->field_alias}, array());
    }
  }
}